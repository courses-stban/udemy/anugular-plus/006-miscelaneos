import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-detalle',
  template: `
    <p>
      usuario-detalle works!
    </p>
  `
})
export class UsuarioDetalleComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.parent.params.subscribe( parametros => {
      console.log("Ruta HIJA Usuario detalle");
      console.log(parametros);
    });
  }

  ngOnInit() {
  }

}
