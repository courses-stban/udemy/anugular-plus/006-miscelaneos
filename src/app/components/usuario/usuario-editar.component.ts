import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-editar',
  template: `
    <p>
      usuario-editar works!
    </p>
  `
})
export class UsuarioEditarComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.parent.params.subscribe( parametros => {
      console.log("Ruta HIJA Usuario editar");
      console.log(parametros);
    });
  }

  ngOnInit() {
  }

}
